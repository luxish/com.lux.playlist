export const environment = {
  production: true,
  API_BASE_PATH: 'playlist-app/api',
  FACEBOOK_CLIENT_ID: '',
  GOOGLE_CLIENT_ID: ''
};
