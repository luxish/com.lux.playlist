import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlaylistService {

  constructor(private httpclient: HttpClient) { 

  }

  fetchVideoItems(tokens: any): Observable<any> {
    return this.httpclient.post("playlist-app/api/posts", tokens);
  }
}
