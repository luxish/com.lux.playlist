import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GoogleContainerComponent } from './google-container/google-container.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    GoogleContainerComponent
  ],
  exports: [
    GoogleContainerComponent
  ]
})
export class GoogleLinkModule { }
