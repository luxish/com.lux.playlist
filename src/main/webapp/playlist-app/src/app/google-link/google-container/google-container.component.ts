import { Component, OnInit, NgZone, EventEmitter, Output } from '@angular/core';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-google-container',
  templateUrl: './google-container.component.html',
  styleUrls: ['./google-container.component.scss']
})
export class GoogleContainerComponent implements OnInit {

  gAPITemplate = {
    client_id: '', 
    fetch_basic_profile: false,
    scope: 'https://www.googleapis.com/auth/youtube.readonly'
  }

  /** 
   *  Emitter on the (login) event.
   */
  @Output() login: EventEmitter<any> = new EventEmitter();

  constructor( 
    private zone: NgZone
    ) { }


  ngOnInit() {
    // Created a window object reference. 
    let windowRef = (window as any);

    this.zone.run(()=> {

      // Setup connection params.
      let initParamas = Object.assign({}, this.gAPITemplate);
      initParamas.client_id = environment.GOOGLE_CLIENT_ID;
      
      windowRef.gapi.load('auth2', function() {
        windowRef.gapi.auth2.init(initParamas);
      })

      // Delegate the response to the component.
      windowRef.onGoogleSignIn = (resp) => {
          this.onGoogleSignIn(resp.getAuthResponse());
      }
    });
  }
  
  /**
   * Method used to signal the login access response.
   * 
   * @param resp the login response
   */
  onGoogleSignIn(resp) {
      if(resp && resp["access_token"]){
        this.login.emit(resp["access_token"]);
      } else {
        console.error("Invalid google api login.")
      }
  }

}
