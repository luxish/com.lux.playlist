import { GoogleLinkModule } from './google-link.module';

describe('GoogleLinkModule', () => {
  let googleLinkModule: GoogleLinkModule;

  beforeEach(() => {
    googleLinkModule = new GoogleLinkModule();
  });

  it('should create an instance', () => {
    expect(googleLinkModule).toBeTruthy();
  });
});
