import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'playlist-app';

  previewList = [];

  ytat: string = "";

  constructor(
    private httpClient: HttpClient
  ) {}

  ngOnInit(): void {
    
  }

  handleYoutubeLogin(token) {
    console.log("TY: " + token);
    this.ytat = token;
  }

  handleFacebookLogin(token) {
     console.log("FB: " + token);
     this.httpClient.post("playlist-app/api/posts", { "fbat": token, "ytat": this.ytat}).subscribe(resp=> {
       console.log(resp);
     });
  }
}
