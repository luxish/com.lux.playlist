import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FacebookLinkModule } from './facebook-link/facebook-link.module';
import { GoogleLinkModule } from './google-link/google-link.module';
import { PlaylistService } from './playlist-service/facebook-api.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FacebookLinkModule,
    GoogleLinkModule
  ],
  providers: [
    PlaylistService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }