import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { FacebookContainerComponent } from './facebook-container/facebook-container.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    HttpClientModule
  ],
  declarations: [
    FacebookContainerComponent
  ],
  exports: [
    FacebookContainerComponent
  ]
})
export class FacebookLinkModule {}
