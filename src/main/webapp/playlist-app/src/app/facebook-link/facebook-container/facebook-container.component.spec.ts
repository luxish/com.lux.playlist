import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacebookContainerComponent } from './facebook-container.component';

describe('FacebookContainerComponent', () => {
  let component: FacebookContainerComponent;
  let fixture: ComponentFixture<FacebookContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacebookContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacebookContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
