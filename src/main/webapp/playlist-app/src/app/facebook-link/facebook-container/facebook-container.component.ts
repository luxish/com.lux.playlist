import { Component, OnInit, NgZone, Output, EventEmitter } from '@angular/core';
import { environment } from '../../../environments/environment';

declare let FB: any;

/**
 * Facebook box component.
 * 
 * Usage:
 *    <app-facebook-container></app-facebook-container>
 */
@Component({
  selector: 'app-facebook-container',
  templateUrl: './facebook-container.component.html',
  styleUrls: ['./facebook-container.component.scss']
})
export class FacebookContainerComponent implements OnInit {

  @Output() login: EventEmitter<any> = new EventEmitter();

  buttonRendered: boolean = false;
  fetched: boolean = false;

  /**
   * API init template.
   */
  apiInitParamsTemplate: any = {
    appId: '',
    status: true,
    xfbml: true,
    autoLogAppEvents: true,
    version: 'v3.1'
  }

  constructor(
    private zone: NgZone,
    private service: FacebookApiService
  ) { }

  /**
   * Component init callback.
   */
  ngOnInit() {
    // Created a window object reference. 
    let windowRef = (window as any);
    
    // The Facebook SDK can be initialized using the  fbAsyncInit hook.
    windowRef.fbAsyncInit = () => {
      let initParams = Object.assign({}, this.apiInitParamsTemplate);
       
      FB.Event.subscribe('xfbml.render', ()=> {
        this.zone.run(() => {
          this.buttonRendered  = true;
        });
      });

      initParams.appId = environment.FACEBOOK_CLIENT_ID;
      FB.init(initParams);
    }

    // This facebook login callback is triggered on FB.login execution. 
    // The method name is set in the html elements data-onlogin attribue.
    windowRef.facebookLoginCallback = () => {
      this.zone.run(() => {
        this.fetchVideoPosts(FB.getAccessToken());
      })
    }

  }

  /**
   * Method used to forward the user's access token.
   *  
   * @param accessToken 
   */
  fetchVideoPosts(accessToken) {
    this.login.emit(accessToken);
   
  }
}
