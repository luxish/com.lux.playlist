import { FacebookLinkModule } from './facebook-link.module';

describe('FacebookLinkModule', () => {
  let facebookLinkModule: FacebookLinkModule;

  beforeEach(() => {
    facebookLinkModule = new FacebookLinkModule();
  });

  it('should create an instance', () => {
    expect(facebookLinkModule).toBeTruthy();
  });
});
