package com.lux.playlist.api;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.lux.playlist.models.PayloadResponse;
import com.lux.playlist.models.PostsRequest;
import com.lux.playlist.services.AccessKeys;
import com.lux.playlist.services.PlayListItem;
import com.lux.playlist.services.PlayListService;

/**
 * Api controller used to transform models to fit the UI and call public APIs for data.
 *
 * @author luxish
 */
@RestController
public class ApplicationBoundary {

    @Autowired
    private PlayListService service;

    /**
     * Method used to retrieve the first 20 posts from the users Facebook wall,
     * filter them for youtube videos and present the list with additional details.
     *
     * @param req {@link PostsRequest}
     * @return {@link PayloadResponse}
     */
    @PostMapping("/api/posts")
    public ResponseEntity<PayloadResponse> getPostsLinks(@RequestBody PostsRequest req) {
        if(req == null) {
            return ResponseEntity.badRequest().build();
        }
        
        List<PlayListItem> resultList = service.retrievePlaylistItems(new AccessKeys(req.fbat, req.ytat));
        return ResponseEntity.ok(createPostLinksResponse(resultList));
    }

    private PayloadResponse createPostLinksResponse(List<PlayListItem> items) {
        PayloadResponse resp = new PayloadResponse();
        // Add all link attributes.
        resp.items.addAll(items);
        resp.size = items.size();
        return resp;
    }

}
