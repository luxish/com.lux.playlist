package com.lux.playlist.models;

import java.util.ArrayList;
import java.util.List;

import com.lux.playlist.services.PlayListItem;

/**
 * POJO class used as a response model.
 */
public class PayloadResponse {

    public int size;

    public List<PlayListItem> items;

    public PayloadResponse() {
        this.size = 0;
        this.items = new ArrayList<>();
    }

}
