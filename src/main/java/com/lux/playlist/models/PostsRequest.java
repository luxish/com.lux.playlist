package com.lux.playlist.models;

/**
 * POJO class used to map the request model.
 */
public class PostsRequest {

    /**
     * The Facebook access token
     */
    public String fbat;
    
    /**
     * The Youtube access token
     */
    public String ytat;

}
