package com.lux.playlist.ext.services.youtube;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Component;

/**
 * 
 * Youtube external service used to call the Google API and transform the message.
 * 
 * @author luxish
 *
 */
@Component
public class YoutubeService {


	/**
	 * Gets all titles for the provided video ids.
	 * 
	 * @param accessToken the access token of the caller.
	 * @param videoIds 
	 * @return a map with pairs <video code - title>
	 */
    public Map<String, String> getTitles(String accessToken,  Set<String> videoIds) {
		YoutubeTemplate youtubeTemplate = new YoutubeTemplate(accessToken);
		Map<String, String> response = new HashMap<>();
		
		youtubeTemplate.getVideosSummary(videoIds).forEach(item-> {
			response.put(item.getId().toString(), item.getSnippet().getTitle());
		});
		return response;
    }
}
