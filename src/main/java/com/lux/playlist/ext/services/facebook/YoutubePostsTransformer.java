package com.lux.playlist.ext.services.facebook;

import org.springframework.social.facebook.api.Post;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Transformer class that converts and filters the input to relevant output objects
 * from where youtube links can be extracted.
 *
 * @author luxish
 */
@Component
public class YoutubePostsTransformer {

    /**
     * Method ues to trasform a list of {@link Post} object into {@link LightPost}.
     *
     * @param list List<Post>
     * @return List<LightPost>
     */
    public List<LightPost> transform(List<Post> list) {
        return list.stream()
                .map(LightPost::new)
                .filter(new LightPostsLinkFilter())
                .peek(item -> System.out.println(item.getLink() + " " +item.getDate()))
                .collect(Collectors.toList());
    }

    /**
     * Class used to provide the filter test for the {@link LightPost} objects.
     *
     *  @author luxsh
     */
    private static class LightPostsLinkFilter implements Predicate<LightPost> {

        /**
         * Links can have the domain << youtube.com >> or << youtu.be >>.
         */
        private static String[] YOUTUBE_KEYS = new String[]{"youtube.com", "youtu.be"};

        /**
         * Tests if the post has a link and is of the preferred form.
         *
         * @param lPost
         * @return true if matches any of the keys.
         */
        @Override
        public boolean test(LightPost lPost) {
            return lPost.getLink() != null && testKeys(lPost.getLink(), YOUTUBE_KEYS);
        }


        boolean testKeys(String val, String[] keys) {
            for(int idx = 0; idx < keys.length; idx++) {
                if(val.contains(keys[idx])) {
                    return true;
                }
            }
            return false;
        }
    }

}
