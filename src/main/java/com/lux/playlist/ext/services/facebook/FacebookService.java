package com.lux.playlist.ext.services.facebook;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.facebook.api.PagingParameters;
import org.springframework.social.facebook.api.Post;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Component;

/**
 * Service class used to interact with the Facebook API.
 *
 *  @author luxish
 */
@Component
public class FacebookService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FacebookService.class);

    @Autowired
    private YoutubePostsTransformer transformer;

    @Autowired
    private FacebookServiceSettings settings;

    /**
     * Private method that sets up the connection with Facebook and performs the search.
     *
     * @param accessToken
     * @return List<LightPost>
     */
    public Set<LightPost> getVideoPosts(String accessToken) {
        // Create a facebook template to access the data using the provided token.
        FacebookTemplate template = new FacebookTemplate(accessToken);

        // Setup dynamic input data.
        int pageCounter = 1;
        long untilDate = referencePointInTime(settings.getMaxMonthsRange()); //limit to be added to the PagingParameters
        int maxResults = settings.getMaxResults();
        int pageSize = settings.getPageSize();
        int maxPages = settings.getMaxPages();
        Set<LightPost> result = new HashSet<>();

        // TODO Fix spring social issue or re-implement it. The second is the better one.
        // FIXME The offset is not taken into account.
		// Query data
        while(result.size() < maxResults && pageCounter == maxPages) {
            List<Post> tmpRetrieved = template.feedOperations().getFeed(createPageParams(pageCounter++, pageSize, untilDate));
            if(tmpRetrieved.size() == 0) {
                break;
            } else {
            	if(tmpRetrieved.size() + result.size() > maxResults) {
            		result.addAll(transformer.transform(tmpRetrieved.subList(0, maxResults - result.size())));
            	} else {
            		result.addAll(transformer.transform(tmpRetrieved));
            	}
            }
            LOGGER.debug("Got page=" + (pageCounter - 1) +" tmpRetrievedSize=" + tmpRetrieved.size() + " resultSize=" + result.size());
        }

        return result;
    }

    /**
     * Creates a {@link PagingParameters} object based on the page number.
     * @param pageNr
     * @param pageSize
     * @param untilDate
     */
    private PagingParameters createPageParams(int pageNr, int pageSize, long untilDate) {
        if(pageNr < 1) {
            LOGGER.error("The page number can not be less than 1. Got: " + pageNr);
            pageNr = 0;
        }
        LOGGER.debug("Offset:" + (pageNr - 1) * pageSize + " Limit:" +pageSize);
        return new PagingParameters(Integer.valueOf(pageSize), Integer.valueOf((pageNr - 1) * pageSize), null, Long.valueOf(untilDate));
    }

    private long referencePointInTime(int maxMonthsRange) {
        return LocalDateTime.now().minusMonths(maxMonthsRange).toInstant(ZoneOffset.UTC).toEpochMilli();
    }
}
