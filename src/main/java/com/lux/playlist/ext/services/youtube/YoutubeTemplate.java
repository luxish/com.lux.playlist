package com.lux.playlist.ext.services.youtube;

import java.io.IOException;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoListResponse;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Youtube bridge implementation that calls Google API. It uses the provided access token for 
 * authorization and google SDK for models and API call implementation.
 * 
 * @author luxish
 *
 */
public class YoutubeTemplate {
	
	private static final String DEFAULT_PART_VALUE = "id,snippet";

	private static final Logger LOGGER = LoggerFactory.getLogger(YoutubeTemplate.class);
	
	private static final JacksonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	private static final NetHttpTransport TRANSPORT = new NetHttpTransport();

	private GoogleCredential credential;
	
	public YoutubeTemplate(String accessToken) {
		credential = new GoogleCredential().setAccessToken(accessToken);
	}
	
	/**
	 * Method used to get video summary information from the Youtube API using the provided
	 * credentials.
	 * 
	 * @param videoIds {@link String} the video id 
	 * @return Video  {@link Video} object. 
	 */
	public Optional<Video> getVideosSummary(String videoId) {
		
		try {
			YouTube youtube = new YouTube.Builder(TRANSPORT, JSON_FACTORY, credential).build();
			
			// The default part is used to query for id and snippet models.
			YouTube.Videos.List request = youtube.videos().list(DEFAULT_PART_VALUE);
			
			VideoListResponse response = request.execute();
			if(response.getItems().size() == 1) {
				return Optional.of(response.getItems().get(0));
			}
			
		} catch (IOException e) {
			LOGGER.error("Could not retrieve the video summaries.", e);
		}
		return Optional.empty();
		
	}
	
	/**
	 * Method used to get video summary information from the Youtube API using the provided
	 * credentials.
	 * 
	 * @param videoIds Set<String> the video id or multiple ids divided by ","
	 * @return Video  {@link Video} object. 
	 */
	public List<Video> getVideosSummary(Set<String> videoIds) {
		
		try {
			YouTube youtube = new YouTube.Builder(TRANSPORT, JSON_FACTORY, credential).build();
			
			// The default part is used to query for id and snippet models.
			YouTube.Videos.List request = youtube.videos().list(DEFAULT_PART_VALUE);
			
			request.setId(String.join(",", videoIds));
			
			VideoListResponse response = request.execute();
			return response.getItems();
			
		} catch (IOException e) {
			LOGGER.error("Could not retrieve the video summaries.", e);
		}
		return Collections.emptyList();
		
	}

}
