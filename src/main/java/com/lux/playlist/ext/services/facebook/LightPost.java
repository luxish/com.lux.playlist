package com.lux.playlist.ext.services.facebook;

import org.springframework.social.facebook.api.Post;

import java.util.Date;

/**
 * A light version of the {@link Post} class. It manipulates required data for {@link FacebookService}.
 *
 * @author luxish
 */
public class LightPost {

    private final String link;

    private final Date creationTime;

    public LightPost(Post post) {
        this.link = post.getLink();
        this.creationTime = post.getCreatedTime();
    }

    public String getLink(){
        return this.link;

    }

    public Date getDate() {
        return this.creationTime;
    }
}
