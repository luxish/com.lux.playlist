package com.lux.playlist.ext.services.facebook;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Settings class used to setup configurable aspects of the {@link FacebookService} behavior.
 *
 * @author luxish
 */
@Component
public class FacebookServiceSettings {

    @Value("${api.facebook.page.size:20}")
    private int pageSize;

    @Value("${api.facebook.max.results:20}")
    private int maxResults;

    @Value("${api.facebook.max.months.range:1}")
    private int maxMonthsRange;

    @Value("${api.facebook.max.pages:1}")
	private int maxPages;

    public FacebookServiceSettings(){}

    public FacebookServiceSettings(int pageSize, int maxResults, int maxMonthsRange, int maxPages){
        this.pageSize = pageSize;
        this.maxResults = maxResults;
        this.maxMonthsRange = maxMonthsRange;
        this.maxPages = maxPages;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getMaxResults() {
        return maxResults;
    }

    public void setMaxResults(int maxResults) {
        this.maxResults = maxResults;
    }

    public int getMaxMonthsRange() {
        return maxMonthsRange;
    }

    public void setMaxMonthsRange(int maxMonthsRange) {
        this.maxMonthsRange = maxMonthsRange;
    }
    
    public void setMaxPages(int maxPages) {
    	this.maxPages = maxPages;
    }

	public int getMaxPages() {
		return maxPages;
	}
}
