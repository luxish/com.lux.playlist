package com.lux.playlist.services;

import com.lux.playlist.ext.services.facebook.LightPost;

public class PlayListItem {
	
	private String title;
	
	private String vCode;
	
	private String vLink;
	
	public PlayListItem(String vCode, String vLink) {
		this.vCode = vCode;
		this.vLink = vLink;
	}
	
	public PlayListItem(String title, String vCode, String vLink) {
		this.title = title;
		this.vCode = vCode;
		this.vLink = vLink;
	}
	
	public static PlayListItemBuilder newBuilder(LightPost post) {
		return new PlayListItemBuilder(post);
	}


	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getvCode() {
		return vCode;
	}

	public void setvCode(String vCode) {
		this.vCode = vCode;
	}

	public String getvLink() {
		return vLink;
	}

	public void setvLink(String vLink) {
		this.vLink = vLink;
	}
	
	public static class PlayListItemBuilder {
		
		private String title;
		
		private String vCode;
		
		private String vLink;
		
		public PlayListItemBuilder(LightPost post) {
			setvLink(post.getLink());
		}
		
		public PlayListItemBuilder setTitle(String title) {
			this.title = title;
			return this;
		}
		
		public PlayListItemBuilder setvCode(String vCode) {
			this.vCode = vCode;
			return this;
		}
		
		public PlayListItemBuilder setvLink(String vLink) {
			this.vLink = vLink;
			return this;
		}
		
		public PlayListItem build() {
			return new PlayListItem(title, vCode, vLink);
		}
	}
	
	

}
