package com.lux.playlist.services;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.api.client.repackaged.com.google.common.base.Splitter;
import com.lux.playlist.ext.services.facebook.FacebookService;
import com.lux.playlist.ext.services.facebook.LightPost;
import com.lux.playlist.ext.services.youtube.YoutubeService;

/**
 * 
 * @author luxish
 * 
 */
@Component
public class PlayListService {
	
	private FacebookService fbService;
	
	private YoutubeService ytService;
	
	@Autowired
	public PlayListService(FacebookService fbService, YoutubeService ytService) {
		this.fbService = fbService;
		this.ytService = ytService;
	}

	/**
	 * Retrieves video posts with additional details.
	 * 
	 * @param keys {@link AccessKeys} object that keeps the access token.
	 * @return List<PlayListItem>
	 */
	public List<PlayListItem> retrievePlaylistItems(AccessKeys keys) {
		Map<String, PlayListItem> map = new HashMap<>();
		
		// Fill the map with items from the facebook wall.
		setPlaylistItems(map, fbService.getVideoPosts(keys.getFacebookAccessToken()));
		// Fill the items with the specific title.
		fillTitlesInPlaylist(map, ytService.getTitles(keys.getYoutubeAccessToken(), map.keySet()));
		
		return map.entrySet().stream().map(item-> item.getValue()).collect(Collectors.toList());
		
	}

	/**
	 * Fill the titles in the source map.
	 * @param map source map
	 * @param titlesMap titles map
	 */
	private void fillTitlesInPlaylist(Map<String, PlayListItem> map, Map<String, String> titlesMap) {
		map.entrySet().forEach(entry -> {
			String title = titlesMap.get(entry.getKey());
			entry.getValue().setTitle(title);
		});
	}

	/**
	 * Sets the playlist items on the source map.
	 * @param map the source map.
	 * @param videoPosts facebook posts
	 */
	private void setPlaylistItems(Map<String, PlayListItem> map, Set<LightPost> videoPosts) {
		videoPosts.forEach(item -> {
			String code = extractCode(item.getLink());
			map.put(code, new PlayListItem(code, item.getLink()));
		});
	}
	
	/**
	 * Method used to extract the video id from an URL.
	 * @param link the URL in string format
	 * @return
	 */
	private String extractCode(String link) {
		if(link == null) {
			return null;			
		}
		try {
			String queryString = (new URL(link)).getQuery();
			Map<String, String> qureyParams =  Splitter.on("&").trimResults().withKeyValueSeparator("=").split(queryString);
			return qureyParams.get("v");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
