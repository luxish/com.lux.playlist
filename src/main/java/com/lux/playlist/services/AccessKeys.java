package com.lux.playlist.services;

public class AccessKeys {
	
	private String facebookAccessToken;
	
	private String youtubeAccessToken;

	public AccessKeys(String facebookAccessToken, String youtubeAccessToken) {
		this.facebookAccessToken = facebookAccessToken;
		this.youtubeAccessToken = youtubeAccessToken;
	}
	
	public String getFacebookAccessToken() {
		return facebookAccessToken;
	}
	
	public String getYoutubeAccessToken() {
		return youtubeAccessToken;
	}

}
